from pointCounter import app
from flaskext.sqlalchemy import SQLAlchemy
from database import *
from flask import render_template, request, session, flash, redirect, url_for


@app.route('/<user_name>', methods=['GET', 'POST'])
def list_data(user_name):
    if 'user_name' in session:
        user = User.query.filter_by(user_name=user_name).first()
        consump_data = Consumption.query.filter_by(user_id=user.uid)
        data = []
        for c in consump_data:
            p = Product.query.filter_by(product_id = c.product_id).first()
            u = Unit.query.filter_by(unit_id = p.unit_id).first()
            cost = c.quantity * p.points_pr_unit
            data.append((p.name, c.quantity, p.points_pr_unit, u.abbr, cost))
        if request.method == 'POST':

            pass

        return render_template('consumption.html', user=user, data=data)
    else:
        redirect(url_for('login'))


@app.route('/')
def index():
    if 'user_name' in session:
        return redirect(url_for('list_data', user_name=session['user_name']))
    else:
        return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        user = User.query.filter_by(user_name=request.form['username']).first()
        if user == None:
            error = 'Invalid input'
        if request.form['username'] != user.user_name:
            error = 'Invalid credentials'
        elif request.form['password'] != user.password:  # All passords must
            error = 'Invalid credentials'                # be hashed
        else:
            session['logged_in'] = True
            session['user_name'] = request.form['username']
            flash('You were logged in')
            return redirect(url_for('list_data', user_name=session['user_name']))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    # remove the username from the session if its there
    session.pop('user_name', None)
    return redirect(url_for('index'))

