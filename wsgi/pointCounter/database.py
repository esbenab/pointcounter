from pointCounter import db

import datetime


class User(db.Model):
    __tablename__ = 'users'

    uid = db.Column(db.Integer, db.Sequence('user_id_seq'), primary_key=True)
    user_name = db.Column(db.String(16))
    full_name = db.Column(db.String(50))
    password = db.Column(db.String(32))
    email = db.Column(db.String(100))
    dob = db.Column(db.Date)

    def __init__(self, user_name, full_name, password, email, dob):
        self.user_name = user_name or email
        self.full_name = full_name
        self.password = password
        self.email = email
        self.dob = dob

    def __repr__(self):
        return "<User('%s', '%s')>" % (self.full_name, self.email)


class Product(db.Model):
    __tablename__ = 'products'

    product_id = db.Column(db.Integer, db.Sequence('products_id_seq'),
            primary_key=True)
    name = db.Column(db.String(60))
    unit_id = db.Column(db.Integer(), db.ForeignKey('units.id'))
    points_pr_unit = db.Column(db.Integer())

    def __init__(self, name, unit_id, points_pr_unit):
        self.name = name
        self.unit_id = unit_id
        self.points_pr_unit = points_pr_unit

    def __repr__(self):
        return "<Product('%s','%d','%d')>" % (self.name, self.unit_id,
            self.points_pr_unit)


class Unit(db.Model):
    __tablename__ = 'units'

    unit_id = db.Column(db.Integer, db.Sequence('units_id_seq'), primary_key=True)
    abbr = db.Column(db.String(5))
    description = db.Column(db.String(50))

    def __init__(self, abbr, description):
        self.abbr = abbr
        self.description = description

    def __repr__(self):
        return "<Units('%s','%s')>" % (self.abbr, self.description)


class Consumption(db.Model):
    __tablename__ = 'consumption'

    consumption_id = db.Column(db.Integer, db.Sequence('consumption_id_seq'),
            primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    product_id = db.Column(db.Integer(), db.ForeignKey('products.id'))
    quantity = db.Column(db.Float())
    time = db.Column(db.DateTime())

    def __init__(self, user_id, product_id, quantity):
        self.time = datetime.now()
        self.user_id = user_id
        self.product_id = product_id
        self.quantity = quantity

    def __repr__(self):
        return "<Consumption('%s','%s','%s')>" % (self.user_id,
                self.product_id, self.quantity)
