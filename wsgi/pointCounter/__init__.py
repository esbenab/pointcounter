from flask import Flask
from flaskext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../../data/pointCounter.db'
app.secret_key = '3\x03\xd0\xd5\x96\x1e\xe0^\x1b\x06\xbb\xc6\xb6\x16?\xc3@\xb2\xa5\x8e\xfci\xa4t'
db = SQLAlchemy(app)

import pointCounter.views

