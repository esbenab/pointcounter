from setuptools import setup

setup(name='pointCounter',
      version='1.0',
      description='OpenShift App',
      author='Esben A Black',
      author_email='esbenab@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=1.7', "SQLAlchemy>=0.7", "Flask-SQLAlchemy"],
)
